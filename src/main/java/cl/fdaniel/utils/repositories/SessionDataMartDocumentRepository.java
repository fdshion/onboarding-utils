package cl.fdaniel.utils.repositories;



import cl.fdaniel.utils.documents.SessionDataMartDocument;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SessionDataMartDocumentRepository extends MongoRepository<SessionDataMartDocument, String> {


}
