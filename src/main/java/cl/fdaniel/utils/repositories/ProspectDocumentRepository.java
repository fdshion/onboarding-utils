package cl.fdaniel.utils.repositories;


import cl.fdaniel.utils.documents.ProspectDocument;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface ProspectDocumentRepository extends MongoRepository<ProspectDocument, String> {

}
