package cl.fdaniel.utils.util;

public class Constants {
    private Constants() {
    }

    public static final String RUT_PATTERN = "^\\d{7,8}+-[\\dkK]$";
    public static final String STR_HYPHEN = "-";
    public static final String STR_K = "k";
    public static final String WRON_RUT = "123-k";
    public static final String EMPTY_STRING = "";
    public static final String INVALID_PASSWORD = "invalidPassword";
    public static final String PASSWORD_LOCKED = "passwordLocked";
    public static final String NO_INFORMATION = "noInformation";


    public static final String PROCESS_TIME = "[%s] -----> ProcessTime %s ms <-----";
    public static final String BEGIN_LOGS = "[%s] >> Begin >>> %s";
    public static final String END_LOGS = "[%s] <<< End << %s ";
    public static final String BEGIN_REST_METHOD = " ----------- RestMethod --------------";
    public static final String END_REST_METHOD = " -------------------------------------";
    public static final String RESULT = "result";

    public static final String STR_DATE_SLASH2 = "yyyy/MM/dd";


    public static final String TRACKING_UPDATE_ISSUES = "Problemas para Actualizar  el tracking: %s";
    public static final int FIRST_ELEMENT = 0;


}
