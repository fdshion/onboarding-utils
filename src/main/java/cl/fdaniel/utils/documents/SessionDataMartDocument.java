package cl.fdaniel.utils.documents;

import lombok.Builder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;


@Builder
@Document(collection = "obd-subsinformationdt")
public record SessionDataMartDocument(
        @Id
        String interactionId,
        @Indexed(unique = true)
        String rut,
        String ipClientWeb,
        String subscriptionStatus,
        String sessionId,
        String sessionStatus,
        LocalDateTime creationDate,
        String uniqueKeyStatus,

        Double realIncome
) {


}
