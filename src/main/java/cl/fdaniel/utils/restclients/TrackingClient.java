package cl.fdaniel.utils.restclients;


import cl.fdaniel.utils.dtos.TrackingRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "trackingClient",
        url = "${rest.endpoints.tracking.url}")
public interface TrackingClient {
    @PostMapping("/ms-obd-customer-relationship-mgmt/v1/tracking")
    void create(  TrackingRequest request);
}
