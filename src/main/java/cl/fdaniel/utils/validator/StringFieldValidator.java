package cl.fdaniel.utils.validator;


import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringFieldValidator implements ConstraintValidator<StringFieldValidation, String> {

    private int length;
    private String name;
    private String regexp;
    private String message;

    @Override
    public void initialize(StringFieldValidation constraintAnnotation) {
        length = constraintAnnotation.length();
        name = constraintAnnotation.fieldName();
        regexp = constraintAnnotation.regexp();
        message = constraintAnnotation.message();
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String request, ConstraintValidatorContext constraintValidatorContext) {

        if (request == null || request.isEmpty()) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(String.format("El Campo %s es Requerido y no puede ser vacio", this.name)).addConstraintViolation();

            return false;
        } else if (request.length() > this.length) {

            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(String.format("La longitud máxima para el Campo %s es de %s de caracteres", this.name, this.length)).addConstraintViolation();
            return false;
        } else {
            Pattern pattern = Pattern.compile(this.regexp);
            Matcher matcher = pattern.matcher(request);

            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(String.format("El Campo %s %s", this.name,this.message)).addConstraintViolation();

            return matcher.matches();
        }


    }
}
