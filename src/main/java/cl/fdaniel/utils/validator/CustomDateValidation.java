package cl.fdaniel.utils.validator;




import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = {CustomDateValidator.class})
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface CustomDateValidation {
    String message() default "Fecha invalida";

    Class<?>[] groups() default {};

    String fieldName() default "";

    Class<? extends Payload>[] payload() default {};

    boolean ignoreCase() default false;

}
