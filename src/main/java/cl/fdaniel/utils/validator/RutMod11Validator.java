package cl.fdaniel.utils.validator;


import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static cl.fdaniel.utils.util.Constants.*;


public class RutMod11Validator implements ConstraintValidator<RutMod11Validation, String> {

    private String name;

    @Override
    public void initialize(RutMod11Validation constraintAnnotation) {
        name = constraintAnnotation.fieldName();
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String rut, ConstraintValidatorContext constraintValidatorContext) {

        if (rut == null || rut.isEmpty()) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(String.format("El Campo %s es Requerido y no puede ser vacio ", this.name)).addConstraintViolation();
            return false;
        }


        Pattern pattern = Pattern.compile(RUT_PATTERN);
        Matcher matcher = pattern.matcher(rut);


        if (matcher.matches()) {
            int seed = 0;
            int residualSigma = 1;
            String[] arrayRut = rut.split(STR_HYPHEN);
            int rutWithOutDigit = Integer.parseInt(arrayRut[0]);
            String rutDigit = arrayRut[1];
            int degenerateRutNumber = rutWithOutDigit;

            while (degenerateRutNumber != 0) {
                residualSigma = (residualSigma + degenerateRutNumber % 10 * (9 - seed++ % 6)) % 11;
                degenerateRutNumber /= 10;
            }

            String verifiedDigit = (residualSigma > 0) ? String.valueOf(residualSigma - 1) : STR_K;

            return rutDigit.equalsIgnoreCase(verifiedDigit);
        } else {

            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(String.format("El Campo %s tiene un formato incorrecto", this.name)).addConstraintViolation();
            return false;
        }


    }
}
