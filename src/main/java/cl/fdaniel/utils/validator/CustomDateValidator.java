package cl.fdaniel.utils.validator;


import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static cl.fdaniel.utils.util.Constants.STR_DATE_SLASH2;


public class CustomDateValidator implements ConstraintValidator<CustomDateValidation, String> {

    private String name;

    @Override
    public void initialize(CustomDateValidation constraintAnnotation) {
        name = constraintAnnotation.fieldName();
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String date, ConstraintValidatorContext constraintValidatorContext) {

        if (date == null || date.isEmpty()) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(String.format("El Campo %s es Requerido y no puede ser vacio ", this.name)).addConstraintViolation();
            return false;
        } else {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(STR_DATE_SLASH2);
            try {
                LocalDate.parse(date, formatter);
                return true;
            } catch (Exception e) {
                return false;
            }
        }
    }
}
