package cl.fdaniel.utils.validator;



import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = {StringFieldValidator.class})
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface StringFieldValidation {

    String message() default "no puede tener caracteres especiales";

    String fieldName() default "String";

    int length() default 60;

    String regexp() default "[\\wÀ-ÿ\\s\\.-]*";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    boolean ignoreCase() default false;
}
