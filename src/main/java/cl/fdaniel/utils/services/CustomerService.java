package cl.fdaniel.utils.services;

import cl.fdaniel.utils.dtos.CustomerRequest;
import cl.fdaniel.utils.dtos.ProspectResponse;

public interface CustomerService {
    ProspectResponse saveCustomer(CustomerRequest request);
}
