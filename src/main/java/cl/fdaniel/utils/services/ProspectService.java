package cl.fdaniel.utils.services;

import cl.fdaniel.utils.dtos.ProspectSpcRequest;
import cl.fdaniel.utils.dtos.ProspectResponse;

public interface ProspectService {
    ProspectResponse saveSpc(ProspectSpcRequest request);
}
