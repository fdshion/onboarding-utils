package cl.fdaniel.utils.services.impl;

import cl.fdaniel.utils.documents.ProspectDocument;
import cl.fdaniel.utils.dtos.ProspectSpcRequest;
import cl.fdaniel.utils.dtos.ProspectResponse;
import cl.fdaniel.utils.repositories.ProspectDocumentRepository;
import cl.fdaniel.utils.services.ProspectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.UUID;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class ProspectServiceImpl implements ProspectService {

    private final ProspectDocumentRepository prospectRepository;

    @Override
    public ProspectResponse saveSpc(ProspectSpcRequest request) {

        String rut = request.rut() == null ? getRandomRut() : request.rut();

        prospectRepository.save(getProspectDocument(request,rut));

        return ProspectResponse
                .builder()
                .interactionId(UUID.randomUUID().toString())
                .rut(rut)
                .build();
    }

    private String getRandomRut() {
        Random random = new Random();
        IntStream intStream = random.ints(1, 5000000, 50000000);


        int seed = 0;
        int residualSigma = 1;
        int rutWithOutDigit = intStream.findFirst().getAsInt();

        int degenerateRutNumber = rutWithOutDigit;

        while (degenerateRutNumber != 0) {
            residualSigma = (residualSigma + degenerateRutNumber % 10 * (9 - seed++ % 6)) % 11;
            degenerateRutNumber /= 10;
        }

        String verifiedDigit = (residualSigma > 0) ? String.valueOf(residualSigma - 1) : "k";

        return String.format("%s-%s", rutWithOutDigit, verifiedDigit);
    }


    private ProspectDocument getProspectDocument(ProspectSpcRequest spcRequest, String rut) {
        String[] arrayRut = rut.split("-");
        String rutWithOutDigit = arrayRut[0];
        String rutDigit = arrayRut[1];
        return ProspectDocument
                .builder()
                .rut(rutWithOutDigit)
                .dv(rutDigit)
                .client("0")
                .Tipo("A")
                .CodigoAsignado("LATHROP")
                .CodigoEnviado("1")
                .Periodo("")
                .Master("3333")
                .Descuento("")
                .Plan("")
                .fidelizacion("")
                .seguros("")
                .TasaConsumo36("")
                .cuota24("")
                .cuota36("")
                .cuota48("")
                .cae24("")
                .cae36("")
                .cae48("")
                .total24("")
                .total36("")
                .total48("")
                .securityPesos("0")
                .ejecutivo("Andres Esteban Espinoza Farias")
                .mailEjecutivo("andres.espinoza @security.cl")
                .Nombre("MARIA FERNANDA")
                .Apellido("ORTUZAR")
                .MontoConsumo(spcRequest.montoConsumo())
                .TC_USD(spcRequest.cupoDolar())
                .cupoMaster(spcRequest.cupoMaster())
                .CupoCuenta(spcRequest.CupoCuenta())
                .Sucursal("METROPOLITANA")
                .Zona("ZONA NORTE")
                .Grupo_Mandatos("")
                .canales("SUCURSAL")
                .Mes_Cargado("202302")
                .SEGMENTO("ACTIVA")
                .RIESGO("4")
                .EMAIL_AGENTE("daniel.giadach @security.cl")
                .NOMBRE_AGENTE("Daniel Giadach Espinoza")
                .NUEVO("1")
                .INVERSIONISTA("0")
                .SEGMENTO1("ACTIVA")
                .EMAIL("fernandaortuzar @gmail.com")
                .EMAIL2("")
                .EMAIL3("")
                .EMAIL4("")
                .EMAIL5("")
                .empresarios("0")
                .LOGIN_1("GRP_SECURITYANDESPINOZA")
                .CODIGO_EJECUTIVO_1("AAM")
                .NOMBRE_EJECUTIVO_1("ANDRES ESTEBAN ESPINOZA FARIAS")
                .EMAIL_EJECUTIVO_1("")
                .NOMBRE_AGENTE_1("")
                .EMAIL_AGENTE_1("")
                .MODULO_1("BANCA ACTIVA 2")
                .MODELO_ATENCION_1("Bca.Activa")
                .SEGMENTO_1("Preferente 3")
                .TC_1_CODIGO("01")
                .TC_1_DESCRIPCION("MasterCard One")
                .TD_1_CODIGO("72 ")
                .TD_1_DESCRIPCION("Debito Activa")
                .RENTA(spcRequest.renta())
                .RENTA_UP_GRADE(spcRequest.rentaUpgrade())
                .LOGIN_2("")
                .CODIGO_EJECUTIVO_2("AAM")
                .NOMBRE_EJECUTIVO_2("CHRISTIAN LOPEZ RIOS")
                .EMAIL_EJECUTIVO_2("")
                .NOMBRE_AGENTE_2("")
                .EMAIL_AGENTE_2("")
                .MODULO_2("PRESIDENTE RIESCO")
                .MODELO_ATENCION_2("Bca.Premium")
                .SEGMENTO_2("Preferente 3")
                .TC_2_CODIGO("02")
                .TC_2_DESCRIPCION("MasterCard Black One")
                .TD_2_CODIGO("71")
                .TD_2_DESCRIPCION("Debito Premium")
                .TC_USD_3("3600")
                .build();
    }
}
