package cl.fdaniel.utils.services.impl;

import cl.fdaniel.utils.documents.SessionDataMartDocument;
import cl.fdaniel.utils.dtos.CustomerRequest;
import cl.fdaniel.utils.dtos.ProspectResponse;
import cl.fdaniel.utils.dtos.TrackingRequest;
import cl.fdaniel.utils.repositories.SessionDataMartDocumentRepository;
import cl.fdaniel.utils.restclients.TrackingClient;
import cl.fdaniel.utils.services.CustomerService;
import cl.fdaniel.utils.services.ProspectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;


@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final ProspectService prospectService;
    private final SessionDataMartDocumentRepository sessionRepository;

    private final TrackingClient trackingClient;

    @Override
    public ProspectResponse saveCustomer(CustomerRequest request) {

        ProspectResponse prospectResponse = prospectService.saveSpc(request.spc());


        SessionDataMartDocument sessionDocument = SessionDataMartDocument
                .builder()
                .interactionId(prospectResponse.interactionId())
                .rut(prospectResponse.rut())
                .subscriptionStatus("subscribing")
                .sessionId(prospectResponse.interactionId())
                .ipClientWeb("10.2.3.4")
                .creationDate(LocalDateTime.now())
                .sessionStatus("")
                .uniqueKeyStatus("")
                .realIncome(3000000.0)
                .build();

        sessionRepository.save(sessionDocument);

        trackingClient.create(TrackingRequest
                .builder()
                .rut(prospectResponse.rut())
                .interactionId(prospectResponse.interactionId())
                .build());





        return prospectResponse;
    }
}
