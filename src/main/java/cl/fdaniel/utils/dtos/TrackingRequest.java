package cl.fdaniel.utils.dtos;

import lombok.Builder;

@Builder
public record TrackingRequest(

        String interactionId,
        String rut
) {
}
