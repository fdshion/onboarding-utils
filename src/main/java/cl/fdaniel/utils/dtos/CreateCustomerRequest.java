package cl.fdaniel.utils.dtos;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;


import java.util.List;

@Builder
public record CreateCustomerRequest(
        @NotBlank(message = "interactionId no puede ser nulo ni vacio")
        String interactionId,


        @Valid
        @NotNull(message = "El campo prospect es requerido")
        Customer prospect,
        @Valid
        @NotNull(message = "El campo selectedProducts es requerido")
        List<Product> selectedProducts


) {
}
