package cl.fdaniel.utils.dtos;


import cl.fdaniel.utils.validator.StringFieldValidation;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Builder;



@Builder
public record Address(


        @StringFieldValidation(fieldName = "streetName", length = 90)
        String streetName,

        @StringFieldValidation(fieldName = "exteriorNumber", length = 9, regexp = "[0-9\\s]*")
        String exteriorNumber,

        @Size(max = 45, message = "internalNumber no puede exceder los 45 caracteres")
        @Pattern(regexp = "[\\wÀ-ÿ\\s\\.-]*", message = "internalNumber no puede tener caracteres especiales ")
        String internalNumber,



        @Pattern(regexp = "[\\wÀ-ÿ\\s\\.-]*", message = "neighborhood no puede tener caracteres especiales")
        @Size(max = 35, message = "neighborhood no puede exceder los 35 caracteres")
        String neighborhood,

        @StringFieldValidation(fieldName = "town", length = 22)
        String town,

        @StringFieldValidation(fieldName = "townCode", length = 4,regexp = "[0-9\\s]{2,4}",message ="solo puede contener números" )
        String townCode,

        @StringFieldValidation(fieldName = "city", length = 15)
        String city,

        @StringFieldValidation(fieldName = "cityCode", length = 4,regexp = "[0-9\\s]{2,4}",message ="solo puede contener números" )
        String cityCode,

        @StringFieldValidation(fieldName = "country", length = 10)
        String country,

        @Valid
        @NotNull(message = "El campo location es requerido")
        Location location

) {
}
