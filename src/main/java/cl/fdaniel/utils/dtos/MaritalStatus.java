package cl.fdaniel.utils.dtos;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import static cl.fdaniel.utils.util.Constants.RUT_PATTERN;

public record MaritalStatus(
        @NotBlank(message = "MaritalStatus.code no puede ser nulo ni vacio")
        @Size(max = 1, message = "MaritalStatus.code no puede exceder 1 caracteres")
        String code,

        @NotBlank(message = "MaritalStatus.description no puede ser nulo ni vacio")
        @Size(max = 45, message = "description no puede exceder los 45 caracteres")
        String description,
        @Pattern(regexp = "[\\wÀ-ÿ\\s.-]*", message = "spouseName no puede tener caracteres especiales")
        @Size(max = 45, message = "spouseName no puede exceder los 45 caracteres")
        String spouseName,
        @Pattern(regexp = RUT_PATTERN, message = "spouseRut no puede tener caracteres especiales")
        String spouseRut
) {
}
