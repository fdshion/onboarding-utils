package cl.fdaniel.utils.dtos;


import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;

public record Location(
        @NotNull(message = "El campo lat es requerido")
        BigDecimal lat,
        @NotNull(message = "El campo lng es requerido")
        BigDecimal lng
) {

}
