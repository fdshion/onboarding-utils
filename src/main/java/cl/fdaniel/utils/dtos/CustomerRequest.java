package cl.fdaniel.utils.dtos;

public record CustomerRequest(
        ProspectSpcRequest spc,
        CreateCustomerRequest client

) {
}
