package cl.fdaniel.utils.dtos;


import cl.fdaniel.utils.validator.CustomDateValidation;
import cl.fdaniel.utils.validator.RutMod11Validation;
import cl.fdaniel.utils.validator.StringFieldValidation;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Builder;



@Builder
public record Customer(

        @StringFieldValidation(fieldName = "firstName", length = 30)
        String firstName,

        @Pattern(regexp = "[\\wÀ-ÿÑñ\\s]*", message = "secondName no puede tener caracteres especiales")
        @Size(max = 30, message = "secondName no puede exceder los 30 caracteres")
        String secondName,


        @StringFieldValidation(fieldName = "firstLastName", length = 30)
        String firstLastName,


        @StringFieldValidation(fieldName = "secondLastName", length = 30)
        String secondLastName,


        @CustomDateValidation(fieldName = "dateOfBirth")
        String dateOfBirth,

        @RutMod11Validation(fieldName = "rut")
        String rut,
        @StringFieldValidation(fieldName = "email", regexp = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$", message = "debe tener formato de correo electrónico válido")
        String email,
        @StringFieldValidation(fieldName = "phoneNumber", regexp = "\\d{8,11}", message = "debe ser un número de telefono")
        String phoneNumber,

        @Valid
        @NotNull(message = "gender es requerido")
        SelectedItem gender,


        @Valid
        @NotNull(message = "maritalStatus es requerido")
        MaritalStatus maritalStatus,

        @Valid
        @NotNull(message = "citizenship es requerido")
        SelectedItem citizenship,

        @Valid
        @NotNull(message = "address es requerido")
        Address address,

        @Valid
        @NotNull(message = "profession es requerido")
        SelectedItem profession,

        @Valid
        @NotNull(message = "workClass es requerido")
        WorkData workClass


) {
}
