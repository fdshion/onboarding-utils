package cl.fdaniel.utils.dtos;

import lombok.Builder;

@Builder
public record ProspectSpcRequest(
        String rut,

        String renta,
        String rentaUpgrade,
        String montoConsumo,
        String cupoDolar,
        String cupoMaster,
        String CupoCuenta


) {
}
