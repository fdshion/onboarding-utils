package cl.fdaniel.utils.dtos;

import lombok.Builder;

@Builder
public record ProspectResponse(
        String interactionId,
        String rut
) {
}
