package cl.fdaniel.utils.dtos;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

public record SelectedItem(

        @NotBlank(message = "code no puede ser nulo ni vacio")
        @Size(max = 3, message = "code no puede exceder los 30 caracteres")
        String code,

        @NotBlank(message = "description no puede ser nulo ni vacio")
        @Size(max = 45, message = "description no puede exceder los 30 caracteres")
        String description
) {
}
