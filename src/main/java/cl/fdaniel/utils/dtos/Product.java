package cl.fdaniel.utils.dtos;


import cl.fdaniel.utils.validator.StringFieldValidation;

public record Product(

        @StringFieldValidation(fieldName = "referenceId", regexp = "[a-zA-Z0-9\\s-]{1,60}")
        String referenceId,
        @StringFieldValidation(fieldName = "productName", regexp = "[\\wÀ-ÿñÑ\\s-!@#$%^&*()_+]{1,60}")
        String productName
) {
}
