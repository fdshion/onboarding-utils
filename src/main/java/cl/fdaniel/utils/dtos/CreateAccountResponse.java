package cl.fdaniel.utils.dtos;

import lombok.Builder;

@Builder
public record CreateAccountResponse(
        String code,
        boolean retry,
        String error
) {
}
