package cl.fdaniel.utils.dtos;


import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import static cl.fdaniel.utils.util.Constants.RUT_PATTERN;

public record WorkData(
        @NotNull(message = "El campo WorkData.type es requerido")
        @Size(max = 20, message = "type no puede exceder los 20 caracteres")
        String type,
        @NotNull(message = "El campo WorkData.code es requerido")
        @Size(max = 1, message = "type no puede exceder los 1 caracteres")
        String code,

        @Pattern(regexp = RUT_PATTERN, message = "employerId no puede tener caracteres especiales")
        String employerId,

        @Size(max = 30, message = "employerName no puede exceder los 30 caracteres")
        @Pattern(regexp = "[\\wÀ-ÿ\\s\\.-]*", message = "employerName no puede tener caracteres especiales ")
        String employerName,

        @Size(max = 15, message = "employerPhone no puede exceder los 15 caracteres")
        @Pattern(regexp = "\\d{8,12}", message = "employerPhone no puede tener caracteres especiales")
        String employerPhone,

        @Size(max = 30, message = "employerAddress no puede exceder los 30 caracteres")
        @Pattern(regexp = "[\\wÀ-ÿ\\s\\.-]*", message = "employerAddress no puede tener caracteres especiales ")
        String employerAddress,
        @Size(max = 45, message = "employerAddressTownName no puede exceder los 30 caracteres")
        @Pattern(regexp = "[\\wÀ-ÿ\\s\\.-]*", message = "employerAddressTownName no puede tener caracteres especiales ")
        String employerAddressTownName,

        @Size(max = 4, message = "employerAddressTownCode no puede exceder los 4 caracteres")
        String employerAddressTownCode,

        String employerAddressTownCityCode,
        String employerAddressTownCityName,
        SelectedItem jobPosition,
        SelectedItem developedActivity

) {
}
