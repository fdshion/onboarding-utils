package cl.fdaniel.utils;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class OnboardingUtilsApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnboardingUtilsApplication.class, args);
    }

}
