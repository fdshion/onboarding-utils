package cl.fdaniel.utils.controller;

import cl.fdaniel.utils.dtos.CustomerRequest;
import cl.fdaniel.utils.dtos.ProspectResponse;
import cl.fdaniel.utils.services.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequiredArgsConstructor
@RequestMapping(CustomerController.CONTEXT_PATH)
public class CustomerController {
    public static final String CONTEXT_PATH = "/v1/customers";
    private final CustomerService customerService;

    @PostMapping
    @ResponseStatus(OK)
    public ProspectResponse saveCustomer(@RequestBody CustomerRequest request) {
        return customerService.saveCustomer(request);
    }
}
