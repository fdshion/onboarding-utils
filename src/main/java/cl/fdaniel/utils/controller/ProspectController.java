package cl.fdaniel.utils.controller;

import cl.fdaniel.utils.dtos.ProspectSpcRequest;
import cl.fdaniel.utils.dtos.ProspectResponse;
import cl.fdaniel.utils.services.ProspectService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequiredArgsConstructor
@RequestMapping(ProspectController.CONTEX_PATH)
public class ProspectController {
    public static final String CONTEX_PATH = "/v1/prospect";
    public static final String SPC = "/spc";

    private final ProspectService prospectService;

    @ResponseStatus(OK)
    @PostMapping(SPC)
    public ProspectResponse saveProspect(@RequestBody ProspectSpcRequest request) {
        return prospectService.saveSpc(request);
    }
}
